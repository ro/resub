use chrono::{Duration, NaiveTime, ParseResult};
use io::{BufWriter, Write};
use std::io::{self, BufRead};
use std::{error::Error, fmt::Display};
use std::{fs::File, path::PathBuf};
use structopt::StructOpt;

const SRT_TIMESTAMP_FMT: &str = "%T,%3f";

#[derive(Debug, StructOpt)]
#[structopt(
    about,
    author,
    after_help = "The idea is that you pick to two points in time in the original subtitle file (--start and --end options) and \
                  map those to the desired correct timestamps (--map-start and --map-end options, respectively). \
                  Every subtitle timestamp is going to be mapped proportionally. \nEffectively, two operations are performed. \
                  Transalation (offset) by (<map_start> - <start>) and scaling by (<map_end> - <map_start>) / (<end> - <start>). \n\
                  If you just want to move all timestamps by fixed offset, specify just --start and --map-start options and \
                  ommit --end and --map-end. \n\
                  Consider a sample scenario. Start playing a movie. The first subtitle in the movie is at 51s, but the scene \
                  starts at 40s. This gives you start options: --start=00:00:51,000 and --map-start=00:00:40,000. \
                  Now scroll towards the end of the movie. You see the subtitle at 10min 51s, but the scene for it is actually at \
                  20min 40s in the movie. This gives you end options: --end=00:10:51,000 and --map-end=00:20:40,000. \
                  We get offset of -11s = 40s - 51s and scale of 2, because orginal time interval of 10min is mapped into 20min. \n\
                  Let's say we have some arbitrary subtitle at 1min 51s. It is going to be mapped to 2min 40s. It is \
                  60s from the --start which is going to be scaled by 2x = 120s and is relative to --map-start, \
                  which gives us 40s + 120s = 160s = 2min 40s."
)]
struct CliOpts {
    /// Timestamp of start of time interval to be mapped from <input_file> file
    /// in SRT format '%H:%M:%S,%3f', e.g. 01:12:33,654
    #[structopt(short = "f", long, parse(try_from_str=parse_time), display_order=1)]
    start: NaiveTime,

    /// Timestamp to map <start> timestamp to in the <output_file> file
    /// in SRT format '%H:%M:%S,%3f', e.g. 01:12:33,654
    #[structopt(short="t", long, parse(try_from_str=parse_time), display_order=2)]
    map_start: NaiveTime,

    /// Timestamp of end of time interval to be mapped from <input_file> file
    /// in SRT format '%H:%M:%S,%3f', e.g. 01:12:33,654
    #[structopt(long, parse(try_from_str=parse_time), requires("map-end"))]
    end: Option<NaiveTime>,

    /// Timestamp to map <end> timestamp to in the <output_file> file
    /// in SRT format '%H:%M:%S,%3f', e.g. 01:12:33,654
    #[structopt(long, parse(try_from_str=parse_time), requires="end")]
    map_end: Option<NaiveTime>,

    /// Input subtitle file
    #[structopt(parse(from_os_str))]
    input_file: PathBuf,

    /// Destination for the subtitles with recalculated (mapped) times
    #[structopt(parse(from_os_str))]
    output_file: PathBuf,
}

fn main() -> Result<(), Box<dyn Error>> {
    let opts = CliOpts::from_args();
    eprintln!("CLI options: {:?}", opts);

    let file = File::open(&opts.input_file)?;
    let lines = io::BufReader::new(file).lines();
    let sub_it = into_subtitle_iter(lines);
    let tsp = TimeStretchParams::new(
        opts.start,
        opts.map_start,
        opts.end
            .unwrap_or_else(|| opts.start + Duration::seconds(1)),
        opts.map_end
            .unwrap_or_else(|| opts.map_start + Duration::seconds(1)),
    );
    eprintln!("Time stretch params: {:?}", &tsp);

    let mut out_file = BufWriter::new(File::create(&opts.output_file)?);

    eprintln!("Remapping subtitles...");

    for sub in sub_it.map(|sub| sub.recalc_timings(&tsp)) {
        write!(out_file, "{}\n\n", sub.to_string())?
    }

    out_file.flush()?;

    eprintln!("Done.");
    Ok(())
}

fn into_subtitle_iter<B>(lines: io::Lines<B>) -> impl Iterator<Item = Subtitle>
where
    B: BufRead,
{
    struct SubIter<B: BufRead> {
        lines: io::Lines<B>,
    }

    impl<B: BufRead> Iterator for SubIter<B> {
        type Item = Subtitle;

        fn next(&mut self) -> Option<Self::Item> {
            parse_subtitle(&mut self.lines)
        }
    }

    SubIter { lines }
}

fn parse_subtitle(lines: &mut impl Iterator<Item = io::Result<String>>) -> Option<Subtitle> {
    let mut lines = lines.skip_while(|rs| if let Ok(s) = rs { s.is_empty() } else { false });

    parse_subtitle_pos(&mut lines).and_then(|pos| {
        parse_subtitle_times(&mut lines).and_then(|(start, end)| {
            parse_subtitle_text(&mut lines).map(|text| Subtitle {
                pos,
                start,
                end,
                text,
            })
        })
    })
}

fn parse_subtitle_pos(lines: &mut impl Iterator<Item = io::Result<String>>) -> Option<u32> {
    lines.next().and_then(|pos_rstr| {
        pos_rstr
            .map_err(|e| eprintln!("Error reading subtitle position field: {}", e))
            .ok()
            .and_then(|pos_str| {
                pos_str
                    .parse::<u32>()
                    .map_err(|e| eprintln!("Error parsing subtitle position field: {}", e))
                    .ok()
            })
    })
}

fn parse_subtitle_times(
    lines: &mut impl Iterator<Item = io::Result<String>>,
) -> Option<(NaiveTime, NaiveTime)> {
    lines.next().and_then(|times_rstr| {
        times_rstr
            .map_err(|e| eprintln!("Error reading subtitle times line: {}", e))
            .ok()
            .and_then(parse_time_line)
    })
}

fn parse_time_line(tl: String) -> Option<(NaiveTime, NaiveTime)> {
    let tsr = tl
        .split(" --> ")
        .take(2)
        .map(parse_time)
        .collect::<chrono::ParseResult<Vec<_>>>();

    match tsr {
        Ok(tsv) => match &tsv[..] {
            [start, end, ..] => Some((*start, *end)),
            _ => {
                eprintln!("Error parsing subtitle time line {}", tl);
                None
            }
        },
        Err(e) => {
            eprintln!("Error parsing subtitle time line {}: {}", tl, e);
            None
        }
    }
}

fn parse_time(ts: &str) -> ParseResult<NaiveTime> {
    NaiveTime::parse_from_str(ts, SRT_TIMESTAMP_FMT)
}

fn parse_subtitle_text(lines: &mut impl Iterator<Item = io::Result<String>>) -> Option<String> {
    let mut lines = lines.by_ref().peekable();
    let is_missing_first_line = match lines.peek() {
        None => true,
        Some(Ok(s)) if s.is_empty() => true,
        _ => false,
    };

    if is_missing_first_line {
        eprintln!(
            "Unexpected EOF parsing subtitle text. Subtitle should have at least one line of text."
        );
        return None;
    }

    let ts = lines
        .take_while(|rs| match rs {
            Ok(s) => !s.is_empty(),
            Err(e) => {
                eprintln!("Error reading subtitle text: {}", e);
                false
            }
        })
        .collect::<io::Result<Vec<_>>>()
        .unwrap();

    if !ts.is_empty() {
        Some(ts.join("\n"))
    } else {
        None
    }
}

#[derive(Debug)]
struct TimeStretchParams {
    t0: NaiveTime,
    tm0: NaiveTime,
    t1: NaiveTime,
    tm1: NaiveTime,
    scale_factor: f64,
}

impl TimeStretchParams {
    fn new(t0: NaiveTime, tm0: NaiveTime, t1: NaiveTime, tm1: NaiveTime) -> Self {
        TimeStretchParams {
            t0,
            tm0,
            t1,
            tm1,
            scale_factor: dur_div(tm1 - tm0, t1 - t0),
        }
    }

    fn recalc_timestamp(&self, t: NaiveTime) -> NaiveTime {
        self.tm0 + dur_mul(t - self.t0, self.scale_factor)
    }
}

fn dur_div(d_num: Duration, d_denom: Duration) -> f64 {
    d_num.num_milliseconds() as f64 / d_denom.num_milliseconds() as f64
}
fn dur_mul(d: Duration, f: f64) -> Duration {
    Duration::milliseconds((d.num_milliseconds() as f64 * f).round() as i64)
}

fn _recalc_timestamp(ti: f32, t0: f32, tm0: f32, t1: f32, tm1: f32) -> f32 {
    tm0 + (ti - t0) * (tm1 - tm0) / (t1 - t0)
}

#[derive(Debug, PartialEq)]
struct Subtitle {
    pos: u32,
    start: NaiveTime,
    end: NaiveTime,
    text: String,
}

impl Subtitle {
    fn recalc_timings(&self, tsp: &TimeStretchParams) -> Subtitle {
        Subtitle {
            pos: self.pos,
            start: tsp.recalc_timestamp(self.start),
            end: tsp.recalc_timestamp(self.end),
            text: self.text.clone(),
        }
    }
}

impl Display for Subtitle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}\n{} --> {}\n{}",
            self.pos,
            self.start.format(SRT_TIMESTAMP_FMT),
            self.end.format(SRT_TIMESTAMP_FMT),
            self.text
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_recalc_timestamp_with_t0_t1() {
        let tmi = _recalc_timestamp(1.5, 1.5, 0.8, 85.4, 97.3);
        assert!(f32_eq(tmi, 0.8));

        let tmi = _recalc_timestamp(85.4, 1.5, 0.8, 85.4, 97.3);
        assert!(f32_eq(tmi, 97.3));
    }

    #[test]
    fn test_recalc_timestamp_before() {
        let tmi = _recalc_timestamp(2.5, 3.0, 1.5, 7.0, 9.5);
        assert!(f32_eq(tmi, 0.5));
    }

    #[test]
    fn test_recalc_timestamp_after() {
        let tmi = _recalc_timestamp(8.5, 3.0, 1.5, 7.0, 9.5);
        assert!(f32_eq(tmi, 12.5));
    }

    #[test]
    fn test_recalc_timestamp_within() {
        let tmi = _recalc_timestamp(4.0, 3.0, 1.5, 7.0, 9.5);
        assert!(f32_eq(tmi, 3.5));
    }

    #[test]
    fn test_tsp_recalc_timestamp_within() {
        let tsp = TimeStretchParams::new(
            NaiveTime::from_hms_milli(0, 0, 3, 0),
            NaiveTime::from_hms_milli(0, 0, 1, 500),
            NaiveTime::from_hms_milli(0, 0, 7, 0),
            NaiveTime::from_hms_milli(0, 0, 9, 500),
        );

        let tmi = tsp.recalc_timestamp(NaiveTime::from_hms_milli(0, 0, 4, 0));

        assert_eq!(tmi, NaiveTime::from_hms_milli(0, 0, 3, 500));
    }

    #[test]
    fn test_recalc_subtitle_timings() {
        let tsp = TimeStretchParams::new(
            NaiveTime::from_hms_milli(0, 0, 3, 0),
            NaiveTime::from_hms_milli(0, 0, 1, 500),
            NaiveTime::from_hms_milli(0, 0, 7, 0),
            NaiveTime::from_hms_milli(0, 0, 9, 500),
        );
        let sub = Subtitle {
            pos: 2,
            start: NaiveTime::from_hms_milli(0, 0, 4, 0),
            end: NaiveTime::from_hms_milli(0, 0, 5, 250),
            text: "sub text".to_owned(),
        };

        let res_sub = sub.recalc_timings(&tsp);

        assert_eq!(
            res_sub,
            Subtitle {
                pos: 2,
                start: NaiveTime::from_hms_milli(0, 0, 3, 500),
                end: NaiveTime::from_hms_milli(0, 0, 6, 0),
                text: "sub text".to_owned(),
            }
        )
    }

    #[test]
    fn test_parse_subtitle_pos() {
        let mut lines = vec![Ok("23".to_owned()), Ok("abc".to_owned())].into_iter();

        let pos = parse_subtitle_pos(&mut lines);

        assert_eq!(pos, Some(23));
        assert_eq!(lines.next().unwrap().unwrap(), "abc");
    }

    #[test]
    fn test_parse_time_line() {
        let times = parse_time_line("00:01:10,160 --> 00:01:13,202".to_owned());

        assert_eq!(
            times,
            Some((
                NaiveTime::from_hms_milli(0, 1, 10, 160),
                NaiveTime::from_hms_milli(0, 1, 13, 202)
            ))
        );
    }

    #[test]
    fn test_parse_subtitle_times() {
        let mut lines = vec![
            Ok("00:01:14,661 --> 00:01:15,869".to_owned()),
            Ok("abc".to_owned()),
        ]
        .into_iter();

        let times = parse_subtitle_times(&mut lines);

        assert_eq!(
            times,
            Some((
                NaiveTime::from_hms_milli(0, 1, 14, 661),
                NaiveTime::from_hms_milli(0, 1, 15, 869)
            ))
        );
        assert_eq!(lines.next().unwrap().unwrap(), "abc");
    }

    #[test]
    fn test_parse_subtitle_text() {
        let mut lines = vec!["line 1", "line 2", "", "abc"]
            .into_iter()
            .map(|s| Ok(s.to_owned()));

        let txt = parse_subtitle_text(&mut lines);

        assert_eq!(txt, Some("line 1\nline 2".to_owned()));
        assert_eq!(lines.next().unwrap().unwrap(), "abc");
    }

    #[test]
    fn test_parse_subtitle_text_at_eof() {
        let mut lines = vec!["line 1", "line 2"]
            .into_iter()
            .map(|s| Ok(s.to_owned()));

        let txt = parse_subtitle_text(&mut lines);

        assert_eq!(txt, Some("line 1\nline 2".to_owned()));
        assert!(lines.next().is_none());
    }

    #[test]
    fn test_parse_subtitle() {
        let mut lines = vec![
            "",
            "23",
            "00:01:14,661 --> 00:01:15,869",
            "line 1",
            "line 2",
            "",
            "abc",
        ]
        .into_iter()
        .map(|s| Ok(s.to_owned()));

        let sub = parse_subtitle(&mut lines);

        assert_eq!(
            sub,
            Some(Subtitle {
                pos: 23,
                start: NaiveTime::from_hms_milli(0, 1, 14, 661),
                end: NaiveTime::from_hms_milli(0, 1, 15, 869),
                text: "line 1\nline 2".to_owned()
            })
        );
        assert_eq!(lines.next().unwrap().unwrap(), "abc");
    }

    #[test]
    fn test_parse_subtitle_at_eof() {
        let mut lines = vec![
            "",
            "",
            "23",
            "00:01:14,661 --> 00:01:15,869",
            "line 1",
            "line 2",
        ]
        .into_iter()
        .map(|s| Ok(s.to_owned()));

        let sub = parse_subtitle(&mut lines);

        assert_eq!(
            sub,
            Some(Subtitle {
                pos: 23,
                start: NaiveTime::from_hms_milli(0, 1, 14, 661),
                end: NaiveTime::from_hms_milli(0, 1, 15, 869),
                text: "line 1\nline 2".to_owned()
            })
        );
        assert!(lines.next().is_none());
    }

    #[test]
    fn test_subtitle_to_string() {
        let sub = Subtitle {
            pos: 23,
            start: NaiveTime::from_hms_milli(0, 1, 14, 661),
            end: NaiveTime::from_hms_milli(0, 1, 15, 869),
            text: "line 1\nline 2".to_owned(),
        };

        assert_eq!(
            sub.to_string(),
            "23\n00:01:14,661 --> 00:01:15,869\nline 1\nline 2"
        );
    }

    #[test]
    fn integration_test_read_subtitles() {
        let file = File::open("tests/short-sample.srt").unwrap();
        let lines = io::BufReader::new(file).lines();
        let mut sub_it = into_subtitle_iter(lines);

        assert_eq!(
            sub_it.next(),
            Some(Subtitle {
                pos: 1,
                start: NaiveTime::from_hms_milli(0, 1, 0, 160),
                end: NaiveTime::from_hms_milli(0, 1, 3, 743),
                text: "I don't see the listing \nfor Bourg-Saint-Maurice.".to_owned()
            })
        );
        assert_eq!(
            sub_it.next(),
            Some(Subtitle {
                pos: 2,
                start: NaiveTime::from_hms_milli(0, 1, 4, 160),
                end: NaiveTime::from_hms_milli(0, 1, 6, 659),
                text: "I'm not surprised,\nthis is Saint-Lazare.".to_owned()
            })
        );
        assert_eq!(
            sub_it.next(),
            Some(Subtitle {
                pos: 3,
                start: NaiveTime::from_hms_milli(0, 1, 6, 660),
                end: NaiveTime::from_hms_milli(0, 1, 9, 744),
                text: "It's written Saint-Lazare \non my ticket.".to_owned()
            })
        );
        assert_eq!(
            sub_it.next(),
            Some(Subtitle {
                pos: 4,
                start: NaiveTime::from_hms_milli(0, 1, 10, 160),
                end: NaiveTime::from_hms_milli(0, 1, 13, 202),
                text: "- Is it my eyes?\n- I think so.".to_owned()
            })
        );
        assert_eq!(
            sub_it.next(),
            Some(Subtitle {
                pos: 5,
                start: NaiveTime::from_hms_milli(0, 1, 14, 661),
                end: NaiveTime::from_hms_milli(0, 1, 15, 869),
                text: "Yeah, it's my eyes.".to_owned()
            })
        );
        assert!(sub_it.next().is_none());
    }

    fn f32_eq(f1: f32, f2: f32) -> bool {
        (f1 - f2).abs() < f32::EPSILON
    }
}
